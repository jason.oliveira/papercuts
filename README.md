# Calculate Paper Cuts

in the vein of Mark Shuttleworth's "100 Papercuts" idea (https://web.archive.org/web/20210508011023/https://launchpad.net/hundredpapercuts), this is a repo of scripts and other files that make Calculate Linux shine and sparkle more than it already does.

### Calculate Linux has problems?    
Yeah. One or five.

### Are these all Calculate problems?    
Nope. Lots of these probably apply to places as varied as SystemD-land. However, if Calculate experiences the paper cut, we will try to mitigate it here.

Don't read this line.
