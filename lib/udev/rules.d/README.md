# Rules are rules, and these are rules.

So, 73-seat-late.rules is important if you want to unplug and replug a controller *after* you've logged into your session. Without it, you have to *log out* and log back in again in order to get your controller to work. Because of a meme a year or two ago about 73-seat-late.rules needing to be deleted in order to fix some other bug in eudev, people now instinctively remove 73-seat-late.rules. **this is bad and shouldn't be done anymore**. As L'Auture (nice dude) buys into this meme and will not reverse his decision even though upstream includes the file, The file is included here.

In addition, as Gentoo and Calculate need a slightly better amount of support for other controllers, 10-local.rules and 60-steam-input.rules are included here, modified by me. They work real good now. If they don't, create an issue.

In the future, Support for Pro Controllers using [dkms-hid-nintendo](https://github.com/nicman23/dkms-hid-nintendo) is planned, as it supports gyro functionality.

### How to use:
just copy the damned rules into /lib/udev/rules.d/ like so:    
sudo cp *.rules /lib/udev/rules.d/    
sudo udevadm control --reload-rules


